<?php
include'DBconnection.php';


$boek = $_POST['boek-f'];
$auteur = $_POST['auteur-f'];
$uitgever = $_POST['uitgever-f'];


$pdoQuery = "INSERT INTO boeken (naam,auteur,uitgever) VALUES (:boek, :auteur, :uitgever)";

$query = $conn->prepare($pdoQuery);

$result = $query->fetch(PDO::FETCH_ASSOC);


$query->execute(array(
    ":boek"=>$boek,
    ":auteur"=>$auteur,
    ":uitgever"=>$uitgever,


));

header('location: ../index.php?page=boekentoevoegen');