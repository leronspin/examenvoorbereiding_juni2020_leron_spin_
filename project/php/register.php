<?php
include'DBconnection.php';


$username = $_POST['username-f'];
$wachtwoord = password_hash($_POST['password-f'],PASSWORD_DEFAULT);
$roleopties = $_POST['role-opties'];

$pdoQuery = "INSERT INTO users (username, password, role) VALUES (:username, :wachtwoord, :role)";

$query = $conn->prepare($pdoQuery);

$result = $query->fetch(PDO::FETCH_ASSOC);


$query->execute(array(
    ":username"=>$username,
    ":wachtwoord"=>$wachtwoord,
    ":role"=>$roleopties

));



header('location: ../index.php?page=home');
