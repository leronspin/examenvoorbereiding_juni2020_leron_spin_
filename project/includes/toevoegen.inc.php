<?php
include 'php/dbconnection.php';
$query = $conn->prepare('SELECT * FROM users');
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);



?>


<!-- Button trigger modal -->
<br>
<div class="container">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Toevoegen
    </button>
    <?php
    if (isset($_SESSION['username-t']))
    echo '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal2"> verwijderen </button>'    
    
    ?>
    <br>
    <br>
    <?php
    if (isset($_SESSION['username-t']))

        echo '<div class="alert alert-success" role="alert">User <strong>'.$_SESSION['username-t'] .'</strong> geladen!</div>'


    ?>
</div>
<!-- Modal toevoegen -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">User toevoegen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="php/modalregister.php" method="post">
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="username-f" class="form-control" placeholder="username" type="text">
                    </div>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="password-f" class="form-control" placeholder="password" type="password">
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="sel1" name="role-opties">
                            <option value="admin">admin</option>
                            <option value="gebruiker">gebruiker</option>
                            <option value="klant">klant</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Toevoegen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
<!-- Modal verwijderen -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <form action="php/deleteuser.php" method="post">
                    <?php
                    echo '<p> Weet u zeker dat u <strong>'.$_SESSION['username-t'] .'</strong> wilt verwijderen?</p>'
                    ?>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Verwijderen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- get user data  -->
<div class="container">
    <div class="row">
        <div class="col-6">
            <div class="list-group" id="list-tab" role="tablist">
                <form action="php/getuserdata.php" method="post">
                    <select class="form-control" id="sel1" name="usernames-t">
                        <option></option>
                        <?php
                        foreach ($result as $row)
                            echo '<option name="" value="' . $row['username'] . '">' . $row['username'] . '</option>';
                        ?>
                    </select>
                    <br>
                    <button type="submit" class="btn btn-primary">laden</button>
                </form>
            </div>
        </div>
        <!-- edit user data -->
        <div class="col-6">
            <form action="php/edituser.php" method="post">
                <div class="tab-content" id="nav-tabContent">
                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">New username:</span>
                        </div>
                        <input type="text" name="edituser" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                    </div>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm">New password:</span>
                            </div>
                            <input type="text" name="editpass" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="sel1" name="role-opties">
                            <option value="admin">admin</option>
                            <option value="gebruiker">gebruiker</option>
                            <option value="klant">klant</option>
                        </select>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary">Aanpassen</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

