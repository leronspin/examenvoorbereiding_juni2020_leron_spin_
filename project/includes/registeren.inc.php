<div class="card bg-light">
    <article class="card-body mx-auto" style="max-width: 400px;">
        <h4 class="card-title mt-3 text-center">Create Account</h4>
        <p class="divider-text"></p>
        <form action="php/register.php" method="post">
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                </div>
                <input name="username-f" class="form-control" placeholder="Username" type="text">
            </div>
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                </div>
                <input name="password-f" class="form-control" placeholder="Password" type="password">
            </div> <!-- form-group// -->
            <div class="form-group">
                <select class="form-control" id="sel1" name="role-opties">
                    <option value="admin">admin</option>
                    <option value="gebruiker">gebruiker</option>
                    <option value="klant">klant</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Create Account</button>
            </div> <!-- form-group// -->
            <p class="text-center">Have an account? <a href="index.php?page=login">Log In</a></p>
        </form>
    </article>
</div> <!-- card.// -->